myApp.controller("Formsctrl", ['$scope', '$http', '$state', '$timeout', '$stateParams', '$rootScope', '$location', function($scope, $http, $state, $timeout, $stateParams, $rootScope, $location) {
    $scope.init = function(){
        
    }
    $scope.errorAlert = false;
    $scope.successAlert = false;
    $scope.saveInfo = function(){
    	var firstname = $scope.firstname;
    	var lastname = $scope.lastname;
    	var username = $scope.username;
    	if(firstname == undefined || lastname == undefined || username == undefined){
    		$scope.errorAlert = true;
    		$scope.errorMsg = "Oops! All fields are mandatory";
    		$timeout(function(){
    			$scope.errorAlert = false;
    			$scope.errorMsg = "";
    		}, 3000);
    		return false;
    	}else{
    		$scope.errorAlert = false;
    		$scope.successAlert = true;
    		$scope.successMsg = "Thank you! Your data is saved successfully!";
    		$timeout(function(){
    			$scope.successAlert = false;
    			$scope.successMsg = "";
    			$scope.firstname = "";
    			$scope.lastname = "";
    			$scope.username = "";
    		}, 3000);
    	}

    }
}]);